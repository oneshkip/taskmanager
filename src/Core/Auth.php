<?php
declare(strict_types=1);

namespace App\Core;

use Illuminate\Support\Arr;

/**
 * Class Auth
 * @package App\Core
 */
class Auth
{
    /**
     * @return bool
     */
    public static function check(): bool
    {
        return Arr::has($_SESSION, 'is_auth');
    }

    public static function auth(): void
    {
        $_SESSION['is_auth'] = true;
    }

    public static function logout(): void
    {
        unset($_SESSION['is_auth']);
    }
}