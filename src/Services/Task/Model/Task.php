<?php

namespace App\Services\Task\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package App\Services\Task\Model
 */
class Task extends Model
{
    protected $fillable = [
        'user_name',
        'email',
        'body',
        'is_done',
        'is_edited',
        'done_at'
    ];
}