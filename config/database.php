<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Arr;

$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => Arr::get($_ENV, 'DB_HOST', 'localhost'),
    'database'  => Arr::get($_ENV, 'DB_NAME', 'default'),
    'username'  => Arr::get($_ENV, 'DB_USER', 'root'),
    'password'  => Arr::get($_ENV, 'DB_PASS', ''),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);
$capsule->setAsGlobal();
$capsule->bootEloquent();