<?php


namespace App\Services\Login;


use Rakit\Validation\Validator;

class LoginValidator
{
    /**
     * @param array $params
     * @return \Rakit\Validation\Validation
     */
    public static function validate(array $params)
    {
        $validator = new Validator;
        $validation = $validator->make($params, [
            'login' => 'required|in:admin',
            'password' => 'required|in:123',
        ], [
            'login:required' => 'Логин должен быть указан',
            'login:in' => 'Неправильный логин',
            'password:required' => 'Пароль должен быть указан',
            'password:in' => 'Неправильный пароль',
        ]);

        $validation->validate();

        return $validation;
    }
}