<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Core\TwigAuth;
use App\Core\TwigSession;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseController
 * @package App\Controllers
 */
abstract class BaseController
{
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param string $view
     * @param array $vars
     * @return mixed
     */
    protected function render(string $view, array $vars = [])
    {
        $template = $this->container->get('view');
        $template->addExtension(new \Twig\Extension\DebugExtension());
        $template->addExtension(new TwigSession());
        $template->addExtension(new TwigAuth());

        $template->addGlobal('session', $_SESSION);
        return $template->render($view, $vars);
    }
}