<?php
declare(strict_types=1);

namespace App\Core;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;

/**
 * Class StringResponseListener
 * @package App\Core
 */
class StringResponseListener implements EventSubscriberInterface
{
    /**
     * @param ViewEvent $event
     */
    public function onView(ViewEvent $event)
    {
        $response = $event->getControllerResult();

        if (is_string($response)) {
            $event->setResponse(new Response($response));
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return ['kernel.view' => 'onView'];
    }
}