<?php
declare(strict_types=1);

namespace App\Services\Task\DTO;

use Illuminate\Support\Arr;

/**
 * Class OrderDTO
 * @package App\Services\Task\DTO
 */
class OrderDTO
{
    private $params;

    /**
     * OrderDTO constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return (int)Arr::get($this->params, 'page', 1);
    }

    /**
     * @return bool
     */
    public function hasOrder(): bool
    {
        return Arr::has($this->params, 'order');
    }

    /**
     * @return string
     */
    public function getOrderField(): string
    {
        return Arr::get($this->params, 'order', 'ASC');
    }

    /**
     * @return string
     */
    public function getOrderDir(): string
    {
        return Arr::get($this->params, 'dir', 'ASC');
    }
}