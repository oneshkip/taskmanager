<?php
declare(strict_types=1);

namespace App\Core;

use App\Controllers\BaseController;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class ControllerResolver
 * @package App\Core
 */
class ControllerResolver extends \Symfony\Component\HttpKernel\Controller\ControllerResolver
{
    protected $container;

    public function __construct(ContainerInterface $container, LoggerInterface $logger = null)
    {
        $this->container = $container;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function createController($controller)
    {
        if (false === strpos($controller, '::') && 2 === substr_count($controller, ':')) {
            // controller in the a:b:c notation then
            $deprecatedNotation = $controller;

            @trigger_error(sprintf('Referencing controllers with %s is deprecated.', $deprecatedNotation, $controller), E_USER_DEPRECATED);
        }

        return parent::createController($controller);
    }

    /**
     * {@inheritdoc}
     */
    protected function instantiateController($class)
    {
        return $this->configureController(parent::instantiateController($class), $class);
    }

    private function configureController($controller, string $class)
    {
        if ($controller instanceof BaseController) {
            if (null === $previousContainer = $controller->setContainer($this->container)) {
                @trigger_error(sprintf('Deprecated. Please configure as service', $class), E_USER_DEPRECATED);
                // To be uncommented on Symfony 5:
                //throw new \LogicException(sprintf('"%s" has no container set, did you forget to define it as a service subscriber?', $class));
            } else {
                $controller->setContainer($previousContainer);
            }
        }

        return $controller;
    }
}