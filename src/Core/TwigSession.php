<?php
declare(strict_types=1);

namespace App\Core;

use Illuminate\Support\Arr;
use Twig\Extension\AbstractExtension;

/**
 * Class TwigSession
 * @package App\Core
 */
class TwigSession extends AbstractExtension
{
    /**
     * @return array|\Twig\TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('flush', [$this, 'flush']),
            new \Twig\TwigFunction('has', [$this, 'has']),
        ];
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function flush($key, $default = null)
    {
        $value = Arr::get($_SESSION, $key, $default);

        Arr::forget($_SESSION, $key);
        return $value;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return Arr::has($_SESSION, $key);
    }
}