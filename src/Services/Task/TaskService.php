<?php
declare(strict_types=1);

namespace App\Services\Task;

use App\Services\Task\DTO\OrderDTO;
use App\Services\Task\Model\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * Class TaskService
 * @package App\Services\Task
 */
class TaskService implements TaskServiceInterface
{
    /**
     * @param array $params
     */
    public function create(array $params): void
    {
        Task::create($params);
    }

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool
    {
        $task = Task::find($id);

        if (!$task) {
            return false;
        }

        if (Arr::has($params, 'body') && $task->is_edited != 1) {
            $params['is_edited'] = $task->body != $params['body'] ? 1 : 0;
        }

        if (Arr::has($params, 'is_done')) {
            if ($task->is_done == 0 && $params['is_done'] == 1) {
                $params['done_at'] = Carbon::now()->toDateTimeString();
            } elseif ($task->is_done == 1 && $params['is_done'] == 0) {
                $params['done_at'] = null;
            }
        }

        $task->fill($params);
        $task->update();

        return true;
    }

    /**
     * @param OrderDTO $order
     * @return mixed
     */
    public function getAll(OrderDTO $order)
    {
        $instance = new Task();

        if ($order->hasOrder()) {
            $instance = $instance->orderBy($order->getOrderField(), $order->getOrderDir());
        } else {
            $instance = $instance->orderBy('id', 'DESC');
        }

        return $instance->paginate(3, ['*'], 'page', $order->getPage());
    }

    /**
     * @param int $id
     * @return Task|null
     */
    public function getById(int $id): ?Task
    {
        return Task::find($id);
    }
}