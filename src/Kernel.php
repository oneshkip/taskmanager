<?php
declare(strict_types=1);

namespace App;

use Symfony\Component\HttpKernel;

/**
 * Class Kernel
 * @package App
 */
class Kernel extends HttpKernel\HttpKernel
{

}