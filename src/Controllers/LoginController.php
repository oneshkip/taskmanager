<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Core\Auth;
use App\Services\Login\LoginValidator;
use App\Services\Task\TaskServiceInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 * @package App\Controllers
 */
class LoginController extends BaseController
{
    /**
     * @return mixed
     */
    public function login()
    {
        if (Auth::check()) {
            return new RedirectResponse('/');
        }

        return $this->render('login.html');
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        if (!Auth::check()) {
            return new RedirectResponse('/');
        }

        Auth::logout();
        return new RedirectResponse('/');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function auth(Request $request)
    {
        if (Auth::check()) {
            return new RedirectResponse('/');
        }

        $validator = LoginValidator::validate($request->request->all());

        if ($validator->fails()) {
            $_SESSION['errors'] = $validator->errors()->toArray();
            return new RedirectResponse('/login');
        }

        Auth::auth();
        return new RedirectResponse('/');
    }
}