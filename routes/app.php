<?php
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();

$routes->add('index', new Route('/', [
    '_controller' => 'App\Controllers\IndexController::index',
], [], [], null, [], ['GET']));

$routes->add('task_store', new Route('/task', [
    '_controller' => 'App\Controllers\TaskController::store'
], [], [], null, [], ['POST']));

$routes->add('task_edit', new Route('/task/{id}', [
    '_controller' => 'App\Controllers\TaskController::edit'
], ['id' => '\d+'], [], null, [], ['GET']));

$routes->add('task_update', new Route('/task/{id}', [
    '_controller' => 'App\Controllers\TaskController::update'
], ['id' => '\d+'], [], null, [], ['POST']));

$routes->add('login_form', new Route('/login', [
    '_controller' => 'App\Controllers\LoginController::login'
], [], [], null, [], ['GET']));

$routes->add('logout', new Route('/logout', [
    '_controller' => 'App\Controllers\LoginController::logout'
], [], [], null, [], ['GET']));

$routes->add('auth', new Route('/auth', [
    '_controller' => 'App\Controllers\LoginController::auth'
], [], [], null, [], ['POST']));

return $routes;