<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Core\Auth;
use App\Services\Task\TaskServiceInterface;
use App\Services\Task\Validator\StoreTaskValidator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TaskController
 * @package App\Controllers
 */
class TaskController extends BaseController
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = StoreTaskValidator::validate($request->request->all());

        if ($validator->fails()) {
            $_SESSION['errors'] = $validator->errors()->toArray();
            return new RedirectResponse('/');
        }

        /** @var TaskServiceInterface $service */
        $service = $this->container->get('task_service');

        try {
            $service->create($request->request->all());
            $_SESSION['success'] = 'Задача добавлена в список';
        } catch (\Exception $ex) {
            $_SESSION['error'] = 'Ошибка при добавлении задачи.';
        }

        return new RedirectResponse('/');
    }

    public function edit(Request $request, $id)
    {
        if (!Auth::check()) {
            return new RedirectResponse('/');
        }

        /** @var TaskServiceInterface $service */
        $service = $this->container->get('task_service');

        $task = $service->getById((int) $id);

        if (!$task) {
            return new RedirectResponse('/');
        }

        return $this->render('task/edit.html', compact('task'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if (!Auth::check()) {
            return new RedirectResponse('/login');
        }

        $validator = StoreTaskValidator::validate($request->request->all());

        if ($validator->fails()) {
            $_SESSION['errors'] = $validator->errors()->toArray();
            return new RedirectResponse('/task/'.$id);
        }

        /** @var TaskServiceInterface $service */
        $service = $this->container->get('task_service');

        try {
            $service->update((int) $id, $request->request->all());
            $_SESSION['success'] = 'Задача успешно обновлена';
        } catch (\Exception $ex) {
            $_SESSION['error'] = 'Ошибка при обновлении задачи.';
        }

        return new RedirectResponse('/task/'.$id);
    }

}