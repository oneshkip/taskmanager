<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Services\Task\DTO\OrderDTO;
use App\Services\Task\TaskServiceInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IndexController
 * @package App\Controllers
 */
class IndexController extends BaseController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $order = (new OrderDTO($request->query->all()));
        /** @var TaskServiceInterface $service */
        $service = $this->container->get('task_service');
        $tasks = $service->getAll($order);

        $queryParams = $request->query->all();

        return $this->render('index.html', compact('tasks', 'queryParams'));
    }
}