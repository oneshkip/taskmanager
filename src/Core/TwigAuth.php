<?php
declare(strict_types=1);

namespace App\Core;

use Twig\Extension\AbstractExtension;

/**
 * Class TwigAuth
 * @package App\Core
 */
class TwigAuth extends AbstractExtension
{
    /**
     * @return array|\Twig\TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig\TwigFunction('auth_check', [$this, 'check']),
        ];
    }

    /**
     * @return bool
     */
    public function check()
    {
        return Auth::check();
    }
}