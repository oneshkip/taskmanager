<?php
declare(strict_types=1);

namespace App\Services\Task;

use App\Services\Task\DTO\OrderDTO;
use App\Services\Task\Model\Task;

/**
 * Interface TaskServiceInterface
 * @package App\Services\Task
 */
interface TaskServiceInterface
{
    /**
     * @param array $params
     */
    public function create(array $params): void;

    /**
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool;

    /**
     * @param OrderDTO $order
     */
    public function getAll(OrderDTO $order);

    /**
     * @param int $id
     * @return Task|null
     */
    public function getById(int $id): ?Task;
}