<?php
declare(strict_types=1);

namespace App\Services\Task\Validator;

use Rakit\Validation\Validator;

/**
 * Class StoreTaskValidator
 * @package App\Services\Task\Validator
 */
class StoreTaskValidator
{
    /**
     * @param array $params
     * @return \Rakit\Validation\Validation
     */
    public static function validate(array $params)
    {
        $validator = new Validator;
        $validation = $validator->make($params, [
            'user_name' => 'required',
            'email' => 'required|email',
            'body' => 'required'
        ], [
            'user_name:required' => 'Имя пользователя должно быть заполнено',
            'email:required' => 'Email должен быть указан',
            'email:email' => 'Неправильный формат email',
            'body:required' => 'Описание задачи должно быть заполнено',
        ]);

        $validation->validate();

        return $validation;
    }
}