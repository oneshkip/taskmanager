<?php

include __DIR__.'/bootstrap/app.php';

return [
    'dbname' => $_ENV['DB_NAME'],
    'user' => $_ENV['DB_USER'],
    'password' => !isset($_ENV['DB_PASS']) ? '' : $_ENV['DB_PASS'],
    'host' => $_ENV['DB_HOST'],
    'driver' => 'pdo_mysql',
];