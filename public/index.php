<?php
include __DIR__.'/../bootstrap/app.php';
require_once __DIR__.'/../bootstrap/app.php';

use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();

$routes = include __DIR__.'/../routes/app.php';
$container = include __DIR__.'/../bootstrap/container.php';

$response = $container->get('framework')->handle($request);
$response->send();